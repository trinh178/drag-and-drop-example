package com.samuiorigin.draganddropexample;

import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.Touch;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

View view = findViewById(R.id.view0);

// Cài drag cho view này khi touch
view.setOnTouchListener((v, event) -> {
    v.performClick();
    if (event.getAction() == MotionEvent.ACTION_DOWN) {
        // Tạo cái bóng trong lúc drag, ở đây tạo bóng theo cái View
        View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v);

        v.startDragAndDrop(null, dragShadowBuilder, v, 0);
        // data là ClipData, ClipData có thể truyền dữ liệu tốt hơn
        // myLocalState truyền vào View để bên DragListener lấy ra thao tác
    }
    return false;
});


view.setOnDragListener((v, event) -> {
    if (event.getAction() == DragEvent.ACTION_DRAG_STARTED) {
        v.setTag(R.id.originTop, (int) event.getX()); // Top
        v.setTag(R.id.originLeft, (int) event.getY()); // Left
    }
    return true;
});


View parent = (View) view.getParent();

// Lắng nghe và thực thi DragDrop, khu vực có thể thực thi DragDrop là parent, nếu kéo
// vượt qua khỏi khu vực này sẽ không hoạt động
/* params:
   v: view lắng nghe ở đây là parent
   event: DragEvent chứa các Action event và dữ liệu view đang drag */
parent.setOnDragListener((v, event) -> {

    // Lấy view đang drag, hồi này chuyền vào localState
    View dragView = (View) event.getLocalState();

    switch (event.getAction()) {
        case DragEvent.ACTION_DRAG_STARTED:
            // Bắt đầu drag

            /*int[] coords = new int[2];
            dragView.getLocationOnScreen(coords);
            int[] coords2 = new int[2];
            v.getLocationOnScreen(coords2);

            dragView.setTag(R.id.originTop, (int) event.getX() - coords[0] - coords2[0]); // Top
            dragView.setTag(R.id.originLeft, (int) event.getY() - coords[1] - coords2[1]); // Left*/

            break;
        case DragEvent.ACTION_DRAG_ENTERED:
            // Điểm drag đã vào bounding box của view, trong trường hợp này view là parent
            break;
        case DragEvent.ACTION_DRAG_LOCATION:
            // Di chuyển trong bounding box
            break;
        case DragEvent.ACTION_DRAG_EXITED:
            // Thoát ra bounding box
            break;
        case DragEvent.ACTION_DROP:
            Log.d("TEST:", "Drop");
            // Thả trong bounding box, thả ngoài bounding box không hoạt động


            // Set vị trí đang kéo cho drag view
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) dragView.getLayoutParams();
            layoutParams.setMargins(
                    (int) event.getX() - (int) dragView.getTag(R.id.originTop),
                    (int) event.getY() - (int) dragView.getTag(R.id.originLeft),
                    0,
                    0
            );
            dragView.setLayoutParams(layoutParams);

            break;
        case DragEvent.ACTION_DRAG_ENDED:
            // Kết thúc drag
            break;
    }
    return true;
});
    }
}
